import sys
import itertools
import re
from unidecode import unidecode
from collections import Counter

class Saviour(object):
    placeholder = "➖"
    internal_placeholder = "#"
    guessed = []
    word = ""
    dictionary = ""
    dict_location = "sources/dictionary.txt"
    saved = False

    def __init__(self, word, letters, placeholder):
        self.placeholder = placeholder
        self.set_word(word)
        guessed = letters.replace(" ", "").strip().split(",")
        self.guessed = list(filter(lambda x: x != '', guessed))
        self.load_dict()
        self.clean_dict()

    def load_dict(self):
        f = open(self.dict_location, "r")
        self.dictionary = unidecode(f.read()).upper()
        f.close()

    def predict_letter(self):
        dict_string = itertools.chain.from_iterable(self.dictionary)
        letters = filter(lambda x: x not in ["\n"," "] + self.guessed, dict_string)
        counts = Counter(list(letters))
        return counts.most_common(1)[0][0]

    def guess(self):
        if not self.internal_placeholder in self.word:
            self.saved = True
            return "nothing, you've won"

        if (len(self.dictionary.split("\n")) == 1):
            self.saved = True
            return self.dictionary
        
        should_guess = self.predict_letter()
        self.guessed += should_guess
        return should_guess

    def create_pattern(self):
        replace_with = self.create_replace()
        pattern = re.sub(self.internal_placeholder, replace_with, self.word)
        return re.compile(r'^{0}$'.format(pattern), re.MULTILINE)

    def set_word(self, word):
        new_word = unidecode(re.sub(self.placeholder, self.internal_placeholder, word))
        if not self.internal_placeholder in new_word:
            self.saved = True
        self.word = new_word

    def create_replace(self):
        if len(self.guessed) == 0:
            return r'[A-Z]'
        else:
            return r'[^' + r''.join(self.guessed) + r']'

    def clean_dict(self):
        pattern = self.create_pattern()
        self.dictionary = "\n".join(re.findall(pattern, self.dictionary))

if __name__ == '__main__':
    word = input("What is the word at this point in the game? ").strip()
    letters = input("What letters have been guessed so far (comma separated)? ").upper()
    placeholder = input("What is the placeholder (press enter for default)? ")
    placeholder = placeholder if placeholder else Saviour.placeholder
    saviour = Saviour(word, letters, placeholder)
    while not saviour.saved:
        print("You should guess: " + saviour.guess())
        if not saviour.saved:
            new_word = ''
            while not len(new_word) is len(saviour.word):
                new_word = input("What is the word after this guess? ").strip()
            saviour.set_word(new_word)
            saviour.clean_dict()
