# Saviour
Saviour is a python script which allows you to cheat at hangman. It was specifically written for [this](https://github.com/Taeir/Galgbot) implementation of hangman, and is distributed with its dictionary.

## Dependencies
 - [Unidecode](https://pypi.org/project/Unidecode/)

## License
Saviour is open source, licensed under the MIT license. See [LICENSE](LICENSE).

Copyright (c) 2018 Bram Crielaard
